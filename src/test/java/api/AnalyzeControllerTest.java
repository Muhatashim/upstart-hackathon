package api;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.mock.web.MockMultipartFile;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;

import static org.hamcrest.Matchers.equalTo;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@RunWith(SpringRunner.class)
@SpringBootTest
@AutoConfigureMockMvc
public class AnalyzeControllerTest {

    @Autowired
    private MockMvc mvc;

    @Test
    public void testAnalyze() throws Exception {
        mvc.perform(MockMvcRequestBuilders.fileUpload("/api/analyze")
                .file(new MockMultipartFile("file", "test file.txt", "text/plain", new byte[0]))
                .accept(MediaType.ALL))
                .andExpect(status().isOk())
                .andExpect(content().string(equalTo("Add the code to analyze test file.txt")));
    }
}
