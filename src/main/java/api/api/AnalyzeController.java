package api.api;

import com.google.cloud.vision.spi.v1.ImageAnnotatorClient;
import com.google.cloud.vision.v1.*;
import com.google.protobuf.ByteString;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import sun.misc.BASE64Decoder;

import java.awt.image.BufferedImage;
import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.util.Arrays;
import java.util.List;

import static java.lang.System.out;

@RestController
@RequestMapping("/api")
public class AnalyzeController {

    @RequestMapping(path = "/analyze", method = RequestMethod.POST, consumes = "multipart/form-data")
    public ResponseEntity<String> analyze(@RequestParam("file") String file) throws IOException {
        BufferedImage image = null;
        byte[] imageByte;

        BASE64Decoder decoder = new BASE64Decoder();
        imageByte = decoder.decodeBuffer(file);
        ByteArrayInputStream bis = new ByteArrayInputStream(imageByte);

        //set image
        Image.Builder vImg = Image.newBuilder().setContent(ByteString.readFrom(bis));
        bis.close();

        Feature feat = Feature.newBuilder().setType(Feature.Type.TEXT_DETECTION).build();
        AnnotateImageRequest req = AnnotateImageRequest.newBuilder().addFeatures(feat).setImage(vImg).build();
        List<AnnotateImageResponse> annotateImagesResponse = ImageAnnotatorClient.create()
                .batchAnnotateImages(Arrays.asList(req)).getResponsesList();


//        for (AnnotateImageResponse res : annotateImagesResponse) {
//            if (res.hasError()) {
//                out.printf("Error: %s\n", res.getError().getMessage());
//                return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
//            }
//
//            // For full list of available annotations, see http://g.co/cloud/vision/docs
//            TextAnnotation annotation = res.getFullTextAnnotation();
//            for (Page page: annotation.getPagesList()) {
//                String pageText = "";
//                for (Block block : page.getBlocksList()) {
//                    String blockText = "";
//                    for (Paragraph para : block.getParagraphsList()) {
//                        String paraText = "";
//                        for (Word word: para.getWordsList()) {
//                            String wordText = "";
//                            for (Symbol symbol: word.getSymbolsList()) {
//                                wordText = wordText + symbol.getText();
//                            }
//                            paraText = paraText + wordText;
//                        }
//                        // Output Example using Paragraph:
//                        out.println("Paragraph: \n" + paraText);
//                        out.println("Bounds: \n" + para.getBoundingBox() + "\n");
//                        blockText = blockText + paraText;
//                    }
//                    pageText = pageText + blockText;
//                }
//            }
//            out.println(annotation.getText());
//        }

        String res = "";
        for (AnnotateImageResponse anAnnotateImagesResponse : annotateImagesResponse) {
//            res += anAnnotateImagesResponse + "\n";
            if (anAnnotateImagesResponse.hasError()) {
                out.printf("Error: %s\n", anAnnotateImagesResponse.getError().getMessage());
                return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
            }

            // For full list of available annotations, see http://g.co/cloud/vision/docs
            TextAnnotation annotation = anAnnotateImagesResponse.getFullTextAnnotation();
            for (Page page : annotation.getPagesList()) {
                String pageText = "";
                for (Block block : page.getBlocksList()) {
                    String blockText = "";
                    for (Paragraph para : block.getParagraphsList()) {
                        String paraText = "";
                        for (Word word : para.getWordsList()) {
                            String wordText = "";
                            for (Symbol symbol : word.getSymbolsList()) {
                                wordText = wordText + symbol.getText();
                            }
                            paraText = paraText + " " + wordText;
                        }
                        // Output Example using Paragraph:
                        out.println("Paragraph: \n" + paraText);
                        out.println("Bounds: \n" + para.getBoundingBox() + "\n");
                        blockText = blockText + " " + paraText;
                    }
                    pageText = pageText + blockText;
                }

                res += pageText + "\n";
            }
            out.println(annotation.getText());
        }

        return new ResponseEntity<>(res, HttpStatus.OK);
    }
}