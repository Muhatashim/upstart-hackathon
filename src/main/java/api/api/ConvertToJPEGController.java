package api.api;

import org.apache.pdfbox.pdmodel.PDDocument;
import org.apache.pdfbox.rendering.PDFRenderer;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;

import javax.imageio.ImageIO;
import javax.xml.bind.DatatypeConverter;
import java.awt.*;
import java.awt.image.RenderedImage;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;

@RestController
@RequestMapping("/api")
public class ConvertToJPEGController {

    @RequestMapping(path = "/convertpdf", method = RequestMethod.POST, consumes = "multipart/form-data")
    public String[] convert(@RequestParam("file") MultipartFile multipartFile) {
        try {
            PDDocument document = PDDocument.load(mpfConvert(multipartFile));

            PDFRenderer renderer = new PDFRenderer(document);

            String[] built64 = new String[document.getNumberOfPages()];

            for (int page = 0; page < document.getNumberOfPages(); ++page) {
                Image image = renderer.renderImageWithDPI(page, 300);

                ByteArrayOutputStream output = new ByteArrayOutputStream();
                ImageIO.write((RenderedImage) image, "jpeg", output);

                built64[page] = DatatypeConverter.printBase64Binary(output.toByteArray());
            }

//                Image image = renderer.renderImageWithDPI(0, 300);
//            ByteArrayOutputStream output = new ByteArrayOutputStream();
//            ImageIO.write((RenderedImage) image, "jpeg", output);
//            return DatatypeConverter.printBase64Binary(output.toByteArray());

            return built64;
        } catch (IOException e) {
            e.printStackTrace();
        }

        return null;
    }

    private File mpfConvert(MultipartFile file) {
        File convFile = new File(file.getOriginalFilename());
        try {
            convFile.createNewFile();
            FileOutputStream fos = new FileOutputStream(convFile);
            fos.write(file.getBytes());
            fos.close();
        } catch (IOException e) {
            e.printStackTrace();
        }

        return convFile;
    }
}
