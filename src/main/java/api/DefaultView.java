package api;

import org.springframework.boot.context.embedded.EmbeddedServletContainerCustomizer;
import org.springframework.boot.web.servlet.ErrorPage;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.http.HttpStatus;

/**
 * Created with IntelliJ IDEA.
 * User: Muhatashim
 * Date: 3/26/2017
 * Time: 5:25 PM
 */
@Configuration
public class DefaultView {

    @Bean
    public EmbeddedServletContainerCustomizer notFoundCustomizer() {
        return container -> container.addErrorPages(
                new ErrorPage(HttpStatus.NOT_FOUND, "/index.html"));
    }
}
